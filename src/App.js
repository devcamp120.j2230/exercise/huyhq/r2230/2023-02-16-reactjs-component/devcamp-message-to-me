import './App.css';
import ContentComponent from './content/ContentComponent';
import TitleComponent from './title/TitleComponent';

function App() {
  return (
    <div className="container">
      <TitleComponent/>
      <br /><br />
      <ContentComponent/>
    </div>
  );
}

export default App;
