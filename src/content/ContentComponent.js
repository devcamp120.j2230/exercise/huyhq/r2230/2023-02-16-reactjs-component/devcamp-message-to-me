import { Component } from "react";
import InputMessage from "./InputMessage";
import LikeImage from "./LikeImage";

class ContentComponent extends Component {
    render() {
        return (
            <>
                <InputMessage/>
                <LikeImage/>
            </>
        )
    }
}

export default ContentComponent;