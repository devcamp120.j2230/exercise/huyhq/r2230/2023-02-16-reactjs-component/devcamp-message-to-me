import { Component } from "react";
import imgLike from '../asset/images/like-yellow-icon.png';

class LikeImage extends Component {
    render() {
        return (
            <>
                <br />
                <p id="show-message">Nội dung thông điệp</p>
                <br />
                <img src={imgLike} id="show-img"></img>
            </>
        )
    }
}

export default LikeImage;