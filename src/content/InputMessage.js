import { Component } from "react";

class InputMessage extends Component {
    onInputChange(e){
        console.log(e.target.value);
    }
    onButtonClick(){
        console.log("Button Click.");
    }
    render() {
        return (
            <>
                <div className="row">
                    <div className="col-25">
                        <label>Thông điệp cho bạn 12 tháng tới</label>
                    </div>
                    <div className="col-25">
                        <input type="text" placeholder="messages cho bạn ..." id="input-message" style={{ width: "420px" }} onChange={this.onInputChange}/>
                    </div>
                </div>
                <div className="row">
                    <button className="button" id="button-cick" onClick={this.onButtonClick}>Gửi thông điệp</button>
                </div>
            </>
        )
    }
}

export default InputMessage;