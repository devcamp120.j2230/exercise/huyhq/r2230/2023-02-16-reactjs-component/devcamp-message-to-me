import React, { Component } from "react";
import TitleImage from "./TitleImage";
import TitleText from "./TitleText";

class TitleComponent extends Component{
    render(){
        return (
            <React.Fragment>
                <TitleText/>
                <TitleImage/>
            </React.Fragment>
        )
    }
}

export default TitleComponent;